import observable from './observable.js'

const SDK_CLIENT_VERSION = '1.0.0'

let CODE_HOME = 'https://maps.locuslabs.com/sdk/dist/current/'

let map = null
let msgId = 1 // unique IDs for each message sent to server
let mapConfig = null;
let isMapInitialized = false;
let isMapRendered = false;

const sentMsgs = { } // track any sent messages to pair up to Promise
let showLogFlag = false

// logging setup
const ts = () => { const d = new Date(); return `${d.getMinutes()}:${d.getSeconds()}.${d.getMilliseconds()}` }
const logPre = [`%cLocusMaps SDK %c(${ts()})%c: `, 'color: orange', 'font-size: 8px', 'font-size: inherit; color: inherit']
const log = function () {
  if (showLogFlag)
    console.log.apply(null, logPre.concat(Array.from(arguments)))
}

function installComs () {
  window.addEventListener('message', msgHandler, false)

  // the map object posts command messages and sets up a responder
  map = (payload, arg2) => {
    if (typeof payload === 'string')
      payload = { ...arg2, command: payload }
    const ob = {
      payload,
      type: 'LL-client',
      msgId: msgId++
    }
    log('Sending command object: ', payload)
    window.postMessage(ob, '*')
    return new Promise((resolve, reject) => {
      sentMsgs[ob.msgId] = { resolve, reject }
    })
  }

  observable(map) // make map handle events pub/sub
}

// this function handles any messages, possibly pairs them up with a sender or triggers an event
function msgHandler (e) {
  const d = e.data
  if (d && d.type === 'LL-server') { // confirm message is from our "server"
    if (d.clientMsgId) {
      if (d.error)
        log('Received error message', d.payload)
      else
        log('Received message', d.payload)

      if (sentMsgs[d.clientMsgId]) {
        if (d.error)
          sentMsgs[d.clientMsgId].reject(d.payload)
        else
          sentMsgs[d.clientMsgId].resolve(d.payload)
      }
    } else
    if (d.event)
      map.fire(d.event, d.payload)
  }
}

// define publicpath for any additional module fetches and insert script for initial main.js
function bootstrapCode (fn) {
  window.LLpublicPath = CODE_HOME
  const scriptNode = document.createElement('script')
  scriptNode.setAttribute('src', CODE_HOME + 'main.js')
  document.head.appendChild(scriptNode)
  scriptNode.addEventListener('load', fn)
}

// this iterates through available commands and makes them member functions of map
function addCommands (ctl, commands) {
  commands.forEach(sig => {
    ctl[sig.command] = function () {
      const cob = { command: sig.command }
      for (let i = 0; i < arguments.length; i++)
        cob[sig.args[i].name] = arguments[i]
      return ctl(cob)
    }
  })
  return ctl
}

// loads the code bundle, sets up coms, and defines a map object for map control
function newMap (renderDiv, config, codeHomeOverride) {

  if (codeHomeOverride)
    CODE_HOME = codeHomeOverride

  return new Promise((resolve, reject) => {
    if (!map) {
      installComs()
      bootstrapCode(() => window.postMessage({ type: 'LL-INIT', config: { ...config, renderDiv } }, '*'))
      map.on('ready', (eName, data) => {
        const { commands/*, customTypes */ } = data.commandJSON

        try {
          map = addCommands(map, commands)
        } catch (e) { reject(e) }

        log('map ready')
        map.observe(function () { const ea = Array.from(arguments); ea.unshift('Event: '); log.apply(null, ea) })
        map.setLogging = LMInit.setLogging
        resolve(map)
      })
    } else
      resolve(map)
  })
}

function loadMap(renderDiv) {
  return new Promise((resolve, reject) => {
    if (!isMapInitialized || !map) {
      log('map is not initialized');
      reject(null);
    }

    if (isMapRendered) {
      resolve(map);
    }

    map.on('ready', (eName, data) => {
      const { commands/*, customTypes */ } = data.commandJSON
  
      try {
        map = addCommands(map, commands)
      } catch (e) { reject(e) }
  
      isMapRendered = true;
  
      log('map ready')
      map.observe(function () { const ea = Array.from(arguments); ea.unshift('Event: '); log.apply(null, ea) })
      map.setLogging = LMInit.setLogging
      resolve(map)
    });

    window.postMessage({ type: 'LL-INIT', config: { ...mapConfig, renderDiv } }, '*');
  })
}

function initialize(config) {
  if (isMapInitialized || map) {
    return;
  }

  isMapInitialized = true;

  mapConfig = config;

  installComs();

  return new Promise((resolve) => {
    bootstrapCode(resolve);
  });
}

const LMInit = {
  getVersion: () => SDK_CLIENT_VERSION,
  initialize,
  loadMap,
  newMap,
  setLogging: flag => { showLogFlag = flag }
}

export default LMInit
