function fire () {
  const ob = this._observers

  if (!ob)
    return undefined

  for (let x = 0; x < ob.length; x++)
    try { ob[x].apply(this, arguments) } catch (err) { console.error(err) }

  return this
}

function observe (cb) {
  if (!this._observers)
    this._observers = []

  this._observers.push(cb)

  return {
    detach: detach.bind(this, cb),
    ob: this
  }
}

function detach (cb) {
  this._observers.splice(this._observers.indexOf(cb), 1)
}

// Returns a new derivitive observer that fires events when the observed object fires iff
// the event passes the predicate function specified here.
function filter (fn) {
  const o2 = create()

  this.observe(function () {
    if (fn.apply(this, arguments))
      o2.fire.apply(o2, arguments)
  })

  return o2
}

// When first argument denotes an event name, this is a convenience
// method to filter by that name
function filterByName (eventName) {
  return filter.call(this, function (name) {
    return eventName === name
  })
}

// Filters on equivilence of first argument (often the event name) and observes
// the result - calling the callback function when triggered
function on (value, cb) {
  return filterByName.call(this, value).observe(cb)
}

const observable = { detach, filter, fire, observe, on }

// extend `to` argument with properties from `from`
// [short version]
function extend (to, from) {
  for (var prop in from)
    to[prop] = from[prop]

  return to
}

function create (ob) {
  if (ob)
    return extend(ob, observable)

  return Object.create(observable)
}

export default create
